# Info

This project is not maintained and may include outdated information or code that is
not compatible with recent versions of tools, such as, Xcode and Clang. The
repository is online for reference only.

For current projects, please visit my [GitHub](https://github.com/shagedorn) or
[Stackoverflow](https://stackoverflow.com/users/2050985/hagi) profiles.

# Parent Project

Please see the REAME of [this repository](https://bitbucket.org/shagedorn/unsafe-api-resolver)
for further information.
